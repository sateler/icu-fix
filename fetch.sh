#!/bin/bash

set -e

builddir=build-icu-jessie

echo "Finding out latest version of icu..."

version=$(rmadison -a source -s jessie icu | cut -f2 -d'|' | tr -d '[[:space:]]')

if [ -z "$version" ] ; then
	echo "Could not find version"
	exit 1
fi

echo "Version found: $version, downloading..."

mkdir -p tmp
pushd tmp

dget --download-only http://httpredir.debian.org/debian/pool/main/i/icu/icu_${version}.dsc

popd

find $builddir -name '*.dsc' -exec dcmd rm '{}' \;

dcmd mv tmp/icu_${version}.dsc $builddir


echo "Downloaded successfully"

tzversion=$(rmadison -a source -s sid tzdata | cut -f2 -d'|' | tr -d '[[:space:]]' | cut -f1 -d-)

echo "Getting tzdata verison $tzversion..."

pushd tmp

wget http://source.icu-project.org/repos/icu/data/trunk/tzdata/icunew/$tzversion/44/{metaZones.txt,timezoneTypes.txt,windowsZones.txt,zoneinfo64.txt}

popd

mv tmp/{metaZones.txt,timezoneTypes.txt,windowsZones.txt,zoneinfo64.txt} $builddir

rmdir tmp
