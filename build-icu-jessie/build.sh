#!/bin/bash

set -e

src=$(ls *.dsc)
dir=$(echo $src | cut -d- -f1 | tr _ -)

if [ ! -f "$src" ] ; then
	echo "Usage: $0 source.dsc"
fi


dpkg-source -x $src

cp metaZones.txt timezoneTypes.txt windowsZones.txt zoneinfo64.txt $dir/source/data/misc/

pushd $dir

sed -i /DEB_MAKE_CHECK_TARGET/d debian/rules

dpkg-buildpackage --target build -j4

# make sure res is updated
pushd source/data
	rm out/build/icudt52l/zoneinfo64.res
	make out/build/icudt52l/zoneinfo64.res
	make
popd

pushd source/samples/date
	make
	off=$(TZ=America/Santiago LD_LIBRARY_PATH=$(pwd)/../../lib ./icudate -r 1461592860 --format XX)
	echo $off
	[ "$off" = "-0300" ]
popd

dch --local=+local "Update tz data"

dpkg-buildpackage -B -nc -us -uc

popd

cp *.deb res/

