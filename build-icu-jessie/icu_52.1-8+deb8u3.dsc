-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: icu
Binary: libicu52, libicu52-dbg, libicu-dev, icu-devtools, icu-doc
Architecture: any all
Version: 52.1-8+deb8u3
Maintainer: Laszlo Boszormenyi (GCS) <gcs@debian.org>
Homepage: http://www.icu-project.org
Standards-Version: 3.9.6
Build-Depends: cdbs (>= 0.4.106~), debhelper (>> 9~), dpkg-dev (>= 1.16.1~), autotools-dev
Build-Depends-Indep: doxygen (>= 1.7.1)
Package-List:
 icu-devtools deb libdevel optional arch=any
 icu-doc deb doc optional arch=all
 libicu-dev deb libdevel optional arch=any
 libicu52 deb libs optional arch=any
 libicu52-dbg deb debug extra arch=any
Checksums-Sha1:
 6de440b71668f1a65a9344cdaf7a437291416781 23875368 icu_52.1.orig.tar.gz
 defbbf4639e70cc75fcde93f12eb7b8b9253337d 28472 icu_52.1-8+deb8u3.debian.tar.xz
Checksums-Sha256:
 2f4d5e68d4698e87759dbdc1a586d053d96935787f79961d192c477b029d8092 23875368 icu_52.1.orig.tar.gz
 aa47fef8f659e6e1ed2a69e1615f5f9ca0b20ed8276fc96c91c0a061f5d12626 28472 icu_52.1-8+deb8u3.debian.tar.xz
Files:
 9e96ed4c1d99c0d14ac03c140f9f346c 23875368 icu_52.1.orig.tar.gz
 63ce326ec2513d01ce820bd5f52ddd9a 28472 icu_52.1-8+deb8u3.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBCAAGBQJV9ysWAAoJENzjEOeGTMi/1pwP/ivXlXiVEL4NQV+dum3Xpj2R
Od68HIKPoRyXMJhCj/IJWeOUeICKFX9pyqMKWIkvpUmukaZMzRx0HLhy4krdn+7t
3Qi3s/2vVP3+G1s1w9Vo7mkPOYWWlileNKv0bPzMS5/t8v8XVnK4dOajgvNi3DxB
tWkmwSAA0UwBnydMbq+myfBD8Mqd7uCR5KaxuTeNCHLmzxEk9WCNi8ZdCa2tSV6N
X3WG5HIRqE1B9bRB0RLClmESYxBa5H87Cu5oZt9di6iVprHfVdKEjMb+HQMlpgEd
gAed+1CFJFgUsLHEr5ZzY5aiyg2IDOEjTACyOrdZ1VnFsSUfvw5OqpDToT/eaV3S
QOpMyXqFwA85W/V5cdy3YsWP6o8DPrlxvBADNvL7MLefSadNJ3f5jjjLlZ2oszsM
PR/bkf4dXHdeoSjJGCdCAEKEDcJ9Cnhv2ME5yB5d2sSwnRHOA5sFnn5WfA93L5kN
R3QOPa6IRYWZXDfSBariPhtpDrTxqRiSnUuKjXjgS0OKaeKXNSHv0+7uTM9Y792X
UWnKkDVbDraJAP14Y0KAIHOJJeys48b/PbQefUgBhmakdrTvd5ulTjQOLPhToaBh
KM7xRyqdut6nNbSdC0OxV4WIy/Fim99u9/CEzaClJ+5PKmgebsZpi5aAkkFjghLB
j6+5Pd4zw+p6+xXO540P
=EQ1Y
-----END PGP SIGNATURE-----
